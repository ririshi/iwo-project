#! /usr/bin/python3
# File name: gendermentions.py
# This script takes a file produced by tweet2tab which contains the 'user',
# 'mentions' and 'text' attributes from a twitter2 database file.
# It counts the number of non-retweet tweets. From those tweets, it counts
# the amount of tweets per gender, and the amount of tweets per gender that
# contain one or more mentions.
# Author: Andele Swierstra
# Date: 27-02-2017

import sys
import re

def no_retweet(tweetlist):
    newlist = []
    total = 0
    with open(tweetlist, encoding = 'utf-8') as tweets:
        for tweet in tweets:
            tweet = tweet.strip().split("\t")
            if not re.search(r'RT @.+', tweet[2]):
                newlist += [(tweet[0], tweet[1])]
            total += 1
    return newlist, total


def find_gender_mentions(tweets):
    female = [ name.strip() for name in \
               open("names/nl-female.txt", encoding = 'utf-8').readlines() ]
    male = [ name.strip() for name in \
             open("names/nl-male.txt", encoding = 'utf-8').readlines() ]
    femaletweets = []
    maletweets = []
    nogender = tweetsf = tweetsm = mentionsf = mentionsm = 0
    for line in tweets:
        name = line[0].lower()
        f = 0
        m = 0
        for n in female:
            if n in name:
                f += 1
        for n in male:
            if n in name:
                m += 1
        if f > m:
            if line[1] != "":
                mentionsf += 1
            tweetsf += 1
        elif m > f:
            if line[1] != "":
                mentionsm += 1
            tweetsm += 1
        else:
            nogender += 1
    return nogender, tweetsm, tweetsf, mentionsm, mentionsf


def main():
    if len(sys.argv) != 2:
        print("Usage:\t{} file, where file is a tweet2tab\n"
              "\toutput file with the attributes 'user' and 'mentions'"
              .format(sys.argv[0]))
        exit(-1)

    tweet_list, total = no_retweet(sys.argv[1])
    nogender, tweetsm, tweetsf, mentionsm, \
    mentionsf = find_gender_mentions(tweet_list)

    print("Total tweets: {}\n"
          "Total non-RT tweets: {}\n"
          "Total genderspecific tweets: {}\n"
          "Male-written tweets: {}\n"
          "Male-written tweets with mentions: {}\n"
          "Percentage of male-written tweets with mentions: {}%\n"
          "Female-written tweets: {}\n"
          "Female-written tweets with mentions: {}\n"
          "Percentage of female-written tweets with mentions: {}%"
          .format(total, len(tweet_list), len(tweet_list) - nogender,
                  tweetsm, mentionsm, round(mentionsm / tweetsm * 100, 1),
                  tweetsf, mentionsf, round(mentionsf / tweetsf * 100, 1)))

if __name__=="__main__":
    main()