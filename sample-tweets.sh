#! /bin/bash

tweets=$(gzip -d < /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | \
         /net/corpora/twitter2/tools/tweet2tab text)
         # Unzips the march 1, 12:00 file and extracts
         # the tweet text using the tweet2tab utility.

echo "Number of tweets: $(wc -l <<< "$tweets")"
# Counts the number of lines in the corpus file.

echo "Number of unique tweets: $(sort -u <<< "$tweets" | wc -l)"
# Sorts the tweets, removes doubles and counts the amount of tweets.

echo "Number of retweets: $(sort -u <<< "$tweets" | grep "^RT @" | wc -l)"
# Searches the tweets for the "RT @" retweet "tag"
# and counts the number of tweets that contain it.

grep -v "^RT @" <<< "$tweets" | awk '!x[$0]++' | head -20
# Searches for tweets that do not contain the retweet "tag",
# and only prints the 20 first tweets that are not duplicates.